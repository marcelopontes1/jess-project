const button = document.getElementById('btn');

let elementClicked = false;

button.addEventListener('click', function handleClick() {
  if (elementClicked) {
    return;
  }

  console.log('element clicked');

  elementClicked = true;
});