const button = document.getElementById('radioTrue');

let elementClicked = false;

button.addEventListener('click', function handleClick() {
  if (elementClicked) {
    return;
  }

  console.log('element clicked');

  elementClicked = true;
})


function myFunctionTrue() {
    document.getElementById("radioFalse").style.backgroundColor = "#fff9e6";
    document.getElementById("radioTrue").style.backgroundColor = "green";
    document.getElementById("demo").innerHTML = "<span>You are right!</span> JavaScript is case-sensitive because it is a basic language that follows a naming convention for the specified objects.";
  }

function myFunctionFalse() {
    document.getElementById("radioTrue").style.backgroundColor = "#fff9e6";
    document.getElementById("radioFalse").style.backgroundColor = "red";
    document.getElementById("demo").innerHTML = "<span>True.</span> JavaScript is case-sensitive because it is a basic language that follows a naming convention for the specified objects.";
  }